﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace s1071632.viewModel
{
    public class BMIDate
    {
        public float? height { get; set; }
        public float? weight { get; set; }
        public float? BMI { get; set; }
        public String LV { get; set; }
    }
}