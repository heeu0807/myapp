﻿using s1071632.viewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace s1071632.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index() {
            return View(new BMIDate());
        }
        // GET: BMI
        [HttpPost]
        public ActionResult Index(BMIDate date)
        {
            if (date.height != null && date.weight != null)
            {
                var m = date.height / 100; 
                var BMI = date.weight / (m * m);
                date.BMI = BMI;
                if (BMI<18.5)
                {
                    date.LV = "體重過輕";
                }
                else if (18.5 <=BMI && BMI<24) {
                    date.LV = "正常範圍";
                }
                else if (24 <= BMI && BMI < 27)
                {
                    date.LV = "過重";
                }
                else if (27<= BMI && BMI < 30)
                {
                    date.LV = "輕度肥胖";
                }
                else if (30<= BMI && BMI < 35)
                {
                    date.LV = "中度肥胖";
                }
                else if (35 <= BMI )
                {
                    date.LV = "重度肥胖";
                }
                
            }
            return View(date);
        }
    }
}